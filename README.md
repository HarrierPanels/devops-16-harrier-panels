# DevOps-7-Harrier-Panels

Both merge and rebase reintegrate new feautures from one branch to another. But
git merge is a safer ooption to keep the entire history of your commits within merging branches while rebase creates a linear history of commits by moving one branch onto the top of another with some commits history just being clean out. 

If a project is large with thousands of commits one should consider rebasing instead of merging unless otherwise to keep the commits history.
<hr>
GIT Basics homework 2: Additional task
 - Make 4 commits at main
 - run git reset --hard HEAD~4
 - restore changes applied during the 3rd commit:
   - git reflog
   - find the 3rd commit id (SHA1)
   - git show 3rd commit id (SHA1)
   - git checkout 3rd commit id (SHA1)
   - git switch -c newbranch
   - git checkout main
   - git merge newbranch (fast-forward merging)
   - delete newbranch if necessary

    
